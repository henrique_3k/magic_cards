import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))


def get_exchange_count(queue):
    channel = connection.channel()
    queue = channel.queue_declare(
        queue=queue, durable=True,
        exclusive=False, auto_delete=False
    )
    return queue.method.message_count


def basic_publish(exchange, routing_key, card):
    channel = connection.channel()
    channel.queue_declare(queue='cards', durable=True)
    channel.basic_publish(exchange=exchange,
                          routing_key=routing_key,
                          body=card,
                          properties=pika.BasicProperties(
                              delivery_mode=2,
                          ))
