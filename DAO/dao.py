import db.connection as con


def get_card_list(expansion_id):
    result = None
    name = None
    db_con = con.create_connection()
    try:
        with db_con.cursor() as cursor:
            sql = f"select *  from magiccard where ExpansionId = {expansion_id};"
            cursor.execute(sql)
            result = cursor.fetchall()
            sql = f"select Name FROM magicexpansion where ExpansionId = {expansion_id};"
            cursor.execute(sql)
            name = cursor.fetchall()

    finally:
        db_con.close()
        return result, name


def get_expansion_card():
    result = None
    db_con = con.create_connection()
    try:
        with db_con.cursor() as cursor:
            sql = f"select ExpansionId from magicexpansion order by ExpansionId asc;"
            cursor.execute(sql)
            result = cursor.fetchall()
    finally:
        db_con.close()
        return result