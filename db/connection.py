import pymysql.cursors
import pymysql


def create_connection():
    connection = pymysql.connect(host='localhost',
                                 user='root',
                                 password='root',
                                 db='tcgplace',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    return connection
