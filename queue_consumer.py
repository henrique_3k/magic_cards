import pika

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))


def on_message(channel, method_frame, header_frame, body):
    with open('/tmp/cards_db.txt', 'a') as f:
        f.write(body.decode("utf-8"))
        f.write("\n")
    channel.basic_ack(delivery_tag=method_frame.delivery_tag)


def read_cards():
    channel = connection.channel()
    channel.basic_consume(on_message, 'cards')
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
    finally:
        print("DONE!")
        channel.stop_consuming()
        channel.close()
        connection.close()


if __name__ == '__main__':
    read_cards()