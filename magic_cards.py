# !/usr/bin/env python3.6
# -*- coding: utf-8 -*-
import json
from threading import Thread
from flask import Flask, jsonify
import DAO.dao as dao
import DAO.rabbitmq as rabbit


app = Flask(__name__)


@app.route('/movecards/<int:expansion_id>', methods=['POST'])
def move_cards(expansion_id, move_all=None):
    card_list, expansion_name = dao.get_card_list(expansion_id)
    count = 0
    for card in card_list:
        body = json.dumps(card)
        rabbit.basic_publish('cards', 'moving_cards', body)
        count += 1
    if move_all is None and len(expansion_name) > 0:
        return jsonify({'Resume': {'expansion_name': expansion_name[0]['Name'], 'count': count}})
    elif move_all:
        return '', 200
    elif len(card_list) == 0:
        return '', 404


@app.route('/get/<string:id_card>', methods=['GET'])
def get_card(id_card):
    status_code = None
    with open('/tmp/cards_db.txt', 'r') as f:
        data = f.read().splitlines()
        for line in data:
            if id_card in line:
                line = json.loads(line)
                return jsonify(line)
            else:
                status_code = 404
        return '', status_code


def move_cards_by_expansion():
    expansion_ids = dao.get_expansion_card()
    for expansion in expansion_ids:
        move_cards(expansion['ExpansionId'], True)


@app.route('/moveall', methods=['GET'])
def move_all():
    thread = Thread(target=move_cards_by_expansion)
    thread.start()
    return '', 202

if __name__ == '__main__':
    app.run(debug=True)
